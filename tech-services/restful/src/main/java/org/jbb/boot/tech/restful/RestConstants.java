/*
 * Copyright (C) 2018 the original author or authors.
 *
 * This file is part of jBB Application Project.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  You may obtain a copy of the License at
 *        http://www.apache.org/licenses/LICENSE-2.0
 */

package org.jbb.boot.tech.restful;

import lombok.experimental.UtilityClass;

@UtilityClass
public final class RestConstants {

    public static final int DOMAIN_REST_CONTROLLER_ADVICE_ORDER = 1;

    public static final String IS_AUTHENTICATED = "isAuthenticated()";
    public static final String IS_AN_ADMINISTRATOR = "hasRole('ROLE_ADMINISTRATOR')";

}
