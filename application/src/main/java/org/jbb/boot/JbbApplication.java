package org.jbb.boot;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class JbbApplication {

    public static void main(String[] args) {
        SpringApplication.run(JbbApplication.class, args);
    }

}
