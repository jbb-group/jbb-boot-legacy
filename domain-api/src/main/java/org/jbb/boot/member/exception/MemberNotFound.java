package org.jbb.boot.member.exception;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

@Getter
@RequiredArgsConstructor
public class MemberNotFound extends Exception {
    private final String memberId;
}
