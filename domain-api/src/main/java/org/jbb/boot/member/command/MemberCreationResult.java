package org.jbb.boot.member.command;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

@Getter
@RequiredArgsConstructor
public class MemberCreationResult {
    private final String memberId;
}
