package org.jbb.boot.member;

import org.jbb.boot.member.temp.BaseEntity;

import javax.persistence.Entity;
import javax.persistence.Table;

import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Tolerate;

@Getter
@Setter
@Entity
@Table(name = "JBB_MEMBERS")
@Builder
@EqualsAndHashCode(callSuper = true)
public class Member extends BaseEntity {

    private String username;

    private String email;

    @Tolerate
    Member() {
        // for JPA...
    }
}
