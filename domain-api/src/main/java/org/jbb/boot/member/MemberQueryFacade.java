package org.jbb.boot.member;

import org.jbb.boot.member.exception.MemberNotFound;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;

public interface MemberQueryFacade {

    Member getMemberById(String memberId) throws MemberNotFound;

    Page<Member> getMembers(PageRequest pageRequest);
}
