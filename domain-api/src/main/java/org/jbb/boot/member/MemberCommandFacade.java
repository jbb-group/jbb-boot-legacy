package org.jbb.boot.member;

import org.jbb.boot.member.command.CreateMemberCommand;
import org.jbb.boot.member.command.MemberCreationResult;
import org.jbb.boot.member.exception.MemberNotFound;

public interface MemberCommandFacade {

    MemberCreationResult createMember(CreateMemberCommand createMemberCommand);

    void deleteMember(String memberId) throws MemberNotFound;

}
