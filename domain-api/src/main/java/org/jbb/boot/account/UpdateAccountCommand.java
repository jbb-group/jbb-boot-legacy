package org.jbb.boot.account;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class UpdateAccountCommand {

    private String email;

}
