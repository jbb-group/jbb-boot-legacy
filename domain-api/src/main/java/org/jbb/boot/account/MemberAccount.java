package org.jbb.boot.account;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class MemberAccount {

    private String memberId;

    private String email;
}
