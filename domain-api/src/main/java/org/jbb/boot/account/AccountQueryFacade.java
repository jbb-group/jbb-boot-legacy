package org.jbb.boot.account;

import org.jbb.boot.member.exception.MemberNotFound;

public interface AccountQueryFacade {

    MemberAccount getAccountForMember(String memberId) throws MemberNotFound;

}
