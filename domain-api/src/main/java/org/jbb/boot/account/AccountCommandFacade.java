package org.jbb.boot.account;

import org.jbb.boot.member.exception.MemberNotFound;

public interface AccountCommandFacade {

    void updateAccountForMember(String memberId, UpdateAccountCommand updateAccountCommand) throws MemberNotFound;

}
