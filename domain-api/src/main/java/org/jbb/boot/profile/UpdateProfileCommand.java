package org.jbb.boot.profile;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class UpdateProfileCommand {

    private String username;

}
