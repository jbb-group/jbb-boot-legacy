package org.jbb.boot.profile;

import org.jbb.boot.member.exception.MemberNotFound;

public interface ProfileCommandFacade {

    void updateProfile(String memberId, UpdateProfileCommand updateProfileCommand) throws MemberNotFound;
}
