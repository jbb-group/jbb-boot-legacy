package org.jbb.boot.profile;

import org.jbb.boot.member.exception.MemberNotFound;

public interface ProfileQueryFacade {

    MemberProfile getProfileForMember(String memberId) throws MemberNotFound;
}
