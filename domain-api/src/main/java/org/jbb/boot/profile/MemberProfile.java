package org.jbb.boot.profile;

import java.time.LocalDateTime;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class MemberProfile {

    private String memberId;

    private LocalDateTime joinDateTime;

    private String username;
}
