package org.jbb.boot.member.web;

import org.jbb.boot.member.Member;
import org.jbb.boot.member.command.CreateMemberCommand;
import org.springframework.stereotype.Component;

@Component
class MemberWebTranslator {

    MemberDto toDto(Member member) {
        return MemberDto.builder()
                .memberId(member.getId())
                .username(member.getUsername())
                .email(member.getEmail())
                .build();
    }

    CreateMemberCommand toCreateCommand(CreateUpdateMemberDto createUpdateMemberDto) {
        return CreateMemberCommand.builder()
                .username(createUpdateMemberDto.getUsername())
                .email(createUpdateMemberDto.getEmail())
                .build();
    }
}
