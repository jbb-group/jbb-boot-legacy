package org.jbb.boot.profile.web;

import org.jbb.boot.profile.MemberProfile;
import org.jbb.boot.profile.UpdateProfileCommand;
import org.springframework.stereotype.Component;

@Component
class ProfileWebTranslator {

    MemberProfileDto toDto(MemberProfile profile) {
        return MemberProfileDto.builder()
                .memberId(profile.getMemberId())
                .joinDateTime(profile.getJoinDateTime())
                .username(profile.getUsername())
                .build();
    }

    UpdateProfileCommand toUpdateProfileCommand(UpdateMemberProfileDto dto) {
        return UpdateProfileCommand.builder()
                .username(dto.getUsername())
                .build();
    }
}
