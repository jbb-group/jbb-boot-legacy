package org.jbb.boot.account.domain;

import org.jbb.boot.account.AccountCommandFacade;
import org.jbb.boot.account.AccountQueryFacade;
import org.jbb.boot.account.MemberAccount;
import org.jbb.boot.account.UpdateAccountCommand;
import org.jbb.boot.member.Member;
import org.jbb.boot.member.MemberQueryFacade;
import org.jbb.boot.member.domain.MemberRepository;
import org.jbb.boot.member.exception.MemberNotFound;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import lombok.RequiredArgsConstructor;

@Service
@RequiredArgsConstructor
class AccountService implements AccountQueryFacade, AccountCommandFacade {
    private final MemberQueryFacade memberQueryFacade;
    private final MemberRepository memberRepository;

    @Override
    @Transactional(readOnly = true)
    public MemberAccount getAccountForMember(String memberId) throws MemberNotFound {
        Member member = memberQueryFacade.getMemberById(memberId);
        return MemberAccount.builder()
                .memberId(member.getId())
                .email(member.getEmail())
                .build();
    }

    @Override
    @Transactional
    public void updateAccountForMember(String memberId, UpdateAccountCommand updateAccountCommand) throws MemberNotFound {
        Member member = memberQueryFacade.getMemberById(memberId);
        member.setEmail(updateAccountCommand.getEmail());
        memberRepository.save(member);
    }
}
