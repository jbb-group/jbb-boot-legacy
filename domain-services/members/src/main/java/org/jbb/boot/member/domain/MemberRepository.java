package org.jbb.boot.member.domain;

import org.jbb.boot.member.Member;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface MemberRepository extends PagingAndSortingRepository<Member, String> {

}
