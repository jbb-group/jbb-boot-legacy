package org.jbb.boot.member.web;

import org.jbb.boot.member.MemberCommandFacade;
import org.jbb.boot.member.MemberQueryFacade;
import org.jbb.boot.member.command.CreateMemberCommand;
import org.jbb.boot.member.command.MemberCreationResult;
import org.jbb.boot.member.exception.MemberNotFound;
import org.jbb.boot.tech.restful.domain.ErrorInfoCodes;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;

import static org.jbb.boot.tech.restful.domain.ErrorInfo.FORBIDDEN;
import static org.jbb.boot.tech.restful.domain.ErrorInfo.MEMBER_NOT_FOUND;
import static org.jbb.boot.tech.restful.domain.ErrorInfo.MISSING_PERMISSION;
import static org.jbb.boot.tech.restful.domain.ErrorInfo.REGISTRATION_FAILED;
import static org.jbb.boot.tech.restful.domain.ErrorInfo.UNAUTHORIZED;
import static org.jbb.boot.tech.restful.domain.RestConstants.API_V1;
import static org.jbb.boot.tech.restful.domain.RestConstants.MEMBERS;
import static org.jbb.boot.tech.restful.domain.RestConstants.MEMBER_ID;
import static org.jbb.boot.tech.restful.domain.RestConstants.MEMBER_ID_VAR;

@RestController
@RequiredArgsConstructor
@RequestMapping(value = API_V1 + MEMBERS, produces = MediaType.APPLICATION_JSON_VALUE)
@Api(tags = API_V1 + MEMBERS)
class MemberResource {

    private final MemberQueryFacade memberQueryFacade;
    private final MemberCommandFacade memberCommandFacade;

    private final MemberWebTranslator webTranslator;

    @PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE)
    @ResponseStatus(HttpStatus.CREATED)
    @ApiOperation("Creates new member")
    @ErrorInfoCodes({REGISTRATION_FAILED})
    public MemberDto createMember(@RequestBody CreateUpdateMemberDto createUpdateMemberDto) throws MemberNotFound {
        CreateMemberCommand createMemberCommand = webTranslator.toCreateCommand(createUpdateMemberDto);
        MemberCreationResult creationResult = memberCommandFacade.createMember(createMemberCommand);
        return webTranslator.toDto(memberQueryFacade.getMemberById(creationResult.getMemberId()));
    }

    @DeleteMapping(MEMBER_ID)
    @ResponseStatus(HttpStatus.NO_CONTENT)
    @ApiOperation("Removes member by id")
    @ErrorInfoCodes({MEMBER_NOT_FOUND, MISSING_PERMISSION, UNAUTHORIZED, FORBIDDEN})
    public void deleteMember(@PathVariable(MEMBER_ID_VAR) String memberId) throws MemberNotFound {
        memberCommandFacade.deleteMember(memberId);
    }
}
