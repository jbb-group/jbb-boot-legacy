package org.jbb.boot.profile.domain;

import org.jbb.boot.member.Member;
import org.jbb.boot.member.MemberQueryFacade;
import org.jbb.boot.member.domain.MemberRepository;
import org.jbb.boot.member.exception.MemberNotFound;
import org.jbb.boot.profile.MemberProfile;
import org.jbb.boot.profile.ProfileCommandFacade;
import org.jbb.boot.profile.ProfileQueryFacade;
import org.jbb.boot.profile.UpdateProfileCommand;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import lombok.RequiredArgsConstructor;

@Service
@RequiredArgsConstructor
class ProfileService implements ProfileQueryFacade, ProfileCommandFacade {
    private final MemberQueryFacade memberQueryFacade;
    private final MemberRepository memberRepository;

    @Override
    @Transactional(readOnly = true)
    public MemberProfile getProfileForMember(String memberId) throws MemberNotFound {
        Member member = memberQueryFacade.getMemberById(memberId);
        return MemberProfile.builder()
                .memberId(member.getId())
                .joinDateTime(member.getCreateDateTime())
                .username(member.getUsername())
                .build();
    }

    @Override
    @Transactional
    public void updateProfile(String memberId, UpdateProfileCommand updateProfileCommand) throws MemberNotFound {
        Member member = memberQueryFacade.getMemberById(memberId);
        member.setUsername(updateProfileCommand.getUsername());
        memberRepository.save(member);
    }
}
