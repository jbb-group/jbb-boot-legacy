package org.jbb.boot.member.web;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
class MemberDto {

    private String memberId;

    private String username;

    private String email;

}
