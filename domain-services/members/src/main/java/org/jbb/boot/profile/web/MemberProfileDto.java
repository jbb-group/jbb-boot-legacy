package org.jbb.boot.profile.web;

import java.time.LocalDateTime;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
class MemberProfileDto {

    private String memberId;

    private LocalDateTime joinDateTime;

    private String username;
}
