package org.jbb.boot.account.web;

import org.jbb.boot.account.AccountCommandFacade;
import org.jbb.boot.account.AccountQueryFacade;
import org.jbb.boot.account.UpdateAccountCommand;
import org.jbb.boot.member.Member;
import org.jbb.boot.member.MemberQueryFacade;
import org.jbb.boot.member.exception.MemberNotFound;
import org.jbb.boot.tech.restful.domain.ErrorInfoCodes;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;

import static org.jbb.boot.tech.restful.domain.ErrorInfo.BAD_CREDENTIALS_WHEN_UPDATE_ACCOUNT;
import static org.jbb.boot.tech.restful.domain.ErrorInfo.GET_NOT_OWN_ACCOUNT;
import static org.jbb.boot.tech.restful.domain.ErrorInfo.MEMBER_NOT_FOUND;
import static org.jbb.boot.tech.restful.domain.ErrorInfo.MISSING_PERMISSION;
import static org.jbb.boot.tech.restful.domain.ErrorInfo.UNAUTHORIZED;
import static org.jbb.boot.tech.restful.domain.ErrorInfo.UPDATE_ACCOUNT_FAILED;
import static org.jbb.boot.tech.restful.domain.ErrorInfo.UPDATE_NOT_OWN_ACCOUNT;
import static org.jbb.boot.tech.restful.domain.RestConstants.ACCOUNT;
import static org.jbb.boot.tech.restful.domain.RestConstants.API_V1;
import static org.jbb.boot.tech.restful.domain.RestConstants.MEMBERS;
import static org.jbb.boot.tech.restful.domain.RestConstants.MEMBER_ID;
import static org.jbb.boot.tech.restful.domain.RestConstants.MEMBER_ID_VAR;

@RestController
@RequiredArgsConstructor
@RequestMapping(value = API_V1 + MEMBERS + MEMBER_ID + ACCOUNT, produces = MediaType.APPLICATION_JSON_VALUE)
@Api(tags = API_V1 + MEMBERS + MEMBER_ID + ACCOUNT)
class MemberAccountResource {
    private final MemberQueryFacade memberQueryFacade;
    private final AccountQueryFacade accountQueryFacade;
    private final AccountCommandFacade accountCommandFacade;

    private final AccountWebTranslator webTranslator;

    @GetMapping
    @ErrorInfoCodes({MEMBER_NOT_FOUND, GET_NOT_OWN_ACCOUNT, UNAUTHORIZED})
    @ApiOperation("Gets member account by member id")
    MemberAccountDto getAccount(@PathVariable(MEMBER_ID_VAR) String memberId) throws MemberNotFound {
        return webTranslator.toDto(accountQueryFacade.getAccountForMember(memberId));
    }

    @PutMapping(consumes = MediaType.APPLICATION_JSON_VALUE)
    @ApiOperation("Updates member account by member id")
    @ErrorInfoCodes({MEMBER_NOT_FOUND, UPDATE_ACCOUNT_FAILED, UPDATE_NOT_OWN_ACCOUNT,
            BAD_CREDENTIALS_WHEN_UPDATE_ACCOUNT, UNAUTHORIZED, MISSING_PERMISSION})
    MemberAccountDto updateAccount(@PathVariable(MEMBER_ID_VAR) String memberId, @RequestBody UpdateMemberAccountDto updateDto) throws MemberNotFound {
        Member member = memberQueryFacade.getMemberById(memberId);
        UpdateAccountCommand updateAccountCommand = webTranslator.toUpdateAccountCommand(updateDto);
        accountCommandFacade.updateAccountForMember(member.getId(), updateAccountCommand);
        return webTranslator.toDto(accountQueryFacade.getAccountForMember(memberId));
    }
}
