package org.jbb.boot.member.domain;

import org.jbb.boot.member.Member;
import org.jbb.boot.member.MemberCommandFacade;
import org.jbb.boot.member.MemberQueryFacade;
import org.jbb.boot.member.command.CreateMemberCommand;
import org.jbb.boot.member.command.MemberCreationResult;
import org.jbb.boot.member.exception.MemberNotFound;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import lombok.RequiredArgsConstructor;

@Service
@RequiredArgsConstructor
class MemberService implements MemberQueryFacade, MemberCommandFacade {
    private final MemberRepository repository;

    @Override
    @Transactional(readOnly = true)
    public Member getMemberById(String memberId) throws MemberNotFound {
        return repository.findById(memberId).orElseThrow(() -> new MemberNotFound(memberId));
    }

    @Override
    @Transactional(readOnly = true)
    public Page<Member> getMembers(PageRequest pageRequest) {
        return repository.findAll(pageRequest);
    }

    @Override
    @Transactional
    public MemberCreationResult createMember(CreateMemberCommand createMemberCommand) {
        Member newMember = Member.builder()
                .username(createMemberCommand.getUsername())
                .email(createMemberCommand.getEmail())
                .build();
        newMember = repository.save(newMember);
        return new MemberCreationResult(newMember.getId());
    }

    @Override
    @Transactional
    public void deleteMember(String memberId) throws MemberNotFound {
        Member member = repository.findById(memberId).orElseThrow(() -> new MemberNotFound(memberId));
        repository.delete(member);
    }
}
