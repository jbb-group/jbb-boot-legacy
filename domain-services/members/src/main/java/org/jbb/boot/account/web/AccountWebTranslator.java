package org.jbb.boot.account.web;

import org.jbb.boot.account.MemberAccount;
import org.jbb.boot.account.UpdateAccountCommand;
import org.springframework.stereotype.Component;

@Component
class AccountWebTranslator {

    MemberAccountDto toDto(MemberAccount account) {
        return MemberAccountDto.builder()
                .memberId(account.getMemberId())
                .email(account.getEmail())
                .build();
    }

    UpdateAccountCommand toUpdateAccountCommand(UpdateMemberAccountDto dto) {
        return UpdateAccountCommand.builder()
                .email(dto.getEmail())
                .build();
    }
}
