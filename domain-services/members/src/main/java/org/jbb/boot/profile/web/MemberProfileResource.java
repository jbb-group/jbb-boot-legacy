package org.jbb.boot.profile.web;

import org.jbb.boot.member.Member;
import org.jbb.boot.member.MemberQueryFacade;
import org.jbb.boot.member.exception.MemberNotFound;
import org.jbb.boot.profile.ProfileCommandFacade;
import org.jbb.boot.profile.ProfileQueryFacade;
import org.jbb.boot.profile.UpdateProfileCommand;
import org.jbb.boot.tech.restful.domain.ErrorInfoCodes;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;

import static org.jbb.boot.tech.restful.domain.ErrorInfo.GET_NOT_OWN_PROFILE;
import static org.jbb.boot.tech.restful.domain.ErrorInfo.MEMBER_NOT_FOUND;
import static org.jbb.boot.tech.restful.domain.ErrorInfo.MISSING_PERMISSION;
import static org.jbb.boot.tech.restful.domain.ErrorInfo.UNAUTHORIZED;
import static org.jbb.boot.tech.restful.domain.ErrorInfo.UPDATE_NOT_OWN_PROFILE;
import static org.jbb.boot.tech.restful.domain.ErrorInfo.UPDATE_PROFILE_FAILED;
import static org.jbb.boot.tech.restful.domain.RestConstants.API_V1;
import static org.jbb.boot.tech.restful.domain.RestConstants.MEMBERS;
import static org.jbb.boot.tech.restful.domain.RestConstants.MEMBER_ID;
import static org.jbb.boot.tech.restful.domain.RestConstants.MEMBER_ID_VAR;
import static org.jbb.boot.tech.restful.domain.RestConstants.PROFILE;

@RestController
@RequiredArgsConstructor
@RequestMapping(value = API_V1 + MEMBERS + MEMBER_ID + PROFILE, produces = MediaType.APPLICATION_JSON_VALUE)
@Api(tags = API_V1 + MEMBERS + MEMBER_ID + PROFILE)
class MemberProfileResource {
    private final MemberQueryFacade memberQueryFacade;
    private final ProfileQueryFacade profileQueryFacade;
    private final ProfileCommandFacade profileCommandFacade;

    private final ProfileWebTranslator webTranslator;

    @GetMapping
    @ErrorInfoCodes({MEMBER_NOT_FOUND, GET_NOT_OWN_PROFILE, UNAUTHORIZED})
    @ApiOperation("Gets member profile by member id")
    MemberProfileDto getProfile(@PathVariable(MEMBER_ID_VAR) String memberId) throws MemberNotFound {
        return webTranslator.toDto(profileQueryFacade.getProfileForMember(memberId));
    }

    @PutMapping(consumes = MediaType.APPLICATION_JSON_VALUE)
    @ErrorInfoCodes({MEMBER_NOT_FOUND, UPDATE_NOT_OWN_PROFILE, UPDATE_PROFILE_FAILED,
            UNAUTHORIZED, MISSING_PERMISSION})
    @ApiOperation("Updates member profile by member id")
    MemberProfileDto updateProfile(@PathVariable(MEMBER_ID_VAR) String memberId, @RequestBody UpdateMemberProfileDto updateDto) throws MemberNotFound {
        Member member = memberQueryFacade.getMemberById(memberId);
        UpdateProfileCommand updateProfileCommand = webTranslator.toUpdateProfileCommand(updateDto);
        profileCommandFacade.updateProfile(member.getId(), updateProfileCommand);
        return webTranslator.toDto(profileQueryFacade.getProfileForMember(member.getId()));
    }
}
