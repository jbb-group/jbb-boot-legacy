package org.jbb.boot.member.web;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
class CreateUpdateMemberDto {

    private String username;

    private String email;

}
